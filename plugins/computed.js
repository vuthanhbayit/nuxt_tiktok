import Vue from 'vue'

Vue.mixin({
  computed: {
    height() {
      return document.body.clientHeight
    }
  }
})
